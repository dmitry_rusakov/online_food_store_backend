# Food Store

## How to install & run

>Note: Before your install project, checkout [Docker](https://docs.docker.com/engine/install/) and [Docker-compose](https://docs.docker.com/compose/install/) install.

1. Clone repository
```
git clone https://dmitry_rusakov@bitbucket.org/dmitry_rusakov/online_food_store_backend.git
```

2. Check docker-compose.yml and .env files

3. Run Docker-compose from the directory, where docker-compose.yml is located. -d option means Detached Mode

```
sudo docker-compose up -d
```

>Note: add 127.0.0.1 food-store.online in /etc/hosts file, then check [food-store.online](http://food-store.online) in browser. You should get `File not found`. It means, that nginx is working, but Drupal is not installed yet.

3. Install all dependencies with `composer install`

>Note: You can make [aliases](https://niklan.net/blog/172#nastroyka-aliasov) to php command

```
sudo docker-compose exec php composer install
```
4. Open site and install Drupal

5. Export config files
```
sudo docker-compose exec php drush cex
```
>Note: If you get error `UUID do not match`, try to replace uuid from exporting config to your local config
```
sudo docker-compose exec php drush cset sistem.site uuid *YOUR_UUID*
```